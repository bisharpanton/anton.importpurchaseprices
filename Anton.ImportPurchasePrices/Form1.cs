﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Ridder.Client.SDK;
using Ridder.Client.SDK.Extensions;
using System.IO;
using OfficeOpenXml;
using System.Runtime.InteropServices.WindowsRuntime;


namespace Anton.ImportPurchasePrices
{
    public partial class Form1 : Form
    {
        private readonly SdkSession _session;

        public Form1()
        {
            _session = new SdkSession();
            InitializeComponent();

            FormClosing += Form1_FormClosing;

            // To report progress from the background worker we need to set this property
            backgroundWorker1.WorkerReportsProgress = true;
            // This event will be raised on the worker thread when the worker starts
            backgroundWorker1.DoWork += new DoWorkEventHandler(BackgroundWorker1_DoWork);
            // This event will be raised when we call ReportProgress
            backgroundWorker1.ProgressChanged += new ProgressChangedEventHandler(BackgroundWorker1_ProgressChanged);
            // This event will be raised when the work is done
            backgroundWorker1.RunWorkerCompleted += BackgroundWorker1_RunWorkerCompleted;
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            Ridder.Common.LoginCompany[] companies;

            try
            {
                companies = _session.Sdk.GetAdministrations();
            }
            catch
            {
                MessageBox.Show("Het is niet gelukt om verbinding te maken met de Ridder-server.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            foreach (var company in companies)
            {
                dataGridView1.Rows.Add(new object[] { company.CompanyName, false });
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog fdlg = new OpenFileDialog
            {
                Filter = "Excel Files|*.xls;*.xlsx;*.xlsm"
            };
            if (fdlg.ShowDialog() == DialogResult.OK)
            {
                // do something here with fdlg.FileName ;
                textBox1.Text = fdlg.FileName;
            }
}

        private void Button2_Click(object sender, EventArgs e)
        {
            if(textBox1.Text=="")
            {
                MessageBox.Show("Geen file gekozen", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            progressBar1.Visible = true;
            progressBar1.Style = ProgressBarStyle.Marquee;
            // Start the background worker
            backgroundWorker1.RunWorkerAsync();

        }


        // On worker thread so do our thing!
        void BackgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            //Werk de voortgangsbalk bij
            List<string> selectedCompanies = new List<string>();
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                if ((bool)row.Cells["Importeer"].Value)
                {
                    selectedCompanies.Add(row.Cells["Bedrijf"].Value.ToString());
                }
            }

            if (selectedCompanies.Count == 0)
            {
                MessageBox.Show("Geen bedrijf geselecteerd", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                Invoke(new MethodInvoker(delegate
                {
                    progressBar1.Style = ProgressBarStyle.Blocks;
                    backgroundWorker1.ReportProgress(0);
                }));

                return;
            }
            double numberOfSteps = (Convert.ToDouble(selectedCompanies.Count) * 3) + 2;
            
            //backgroundWorker1.ReportProgress((int)Math.Round(100 / numberOfSteps, 0)); //Eerste stap
            /*Invoke(new MethodInvoker(delegate
            {
                label2.Text = "Uitlezen Excel";
            }));*/


            //1. - Lees de Excel-file uit
            string fileName = textBox1.Text;

            IList<ExcelRow> rows = new List<ExcelRow>();
            ExcelPackage excelPackage;

            try
            {
                excelPackage = new ExcelPackage(new FileInfo(fileName));
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Import niet gelukt; foutmelding:{ex.Message}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                Invoke(new MethodInvoker(delegate
                {
                    progressBar1.Style = ProgressBarStyle.Blocks;
                    backgroundWorker1.ReportProgress(0);
                }));

                return;
            }
            var worksheet = excelPackage.Workbook.Worksheets.FirstOrDefault();

            for (int rowNum = 2; rowNum <= worksheet.Dimension.End.Row; rowNum++)
            {
                var row = new ExcelRow
                {
                    Itemcode = worksheet.Cells["A" + rowNum].Text.ToUpper(), //De code staat altijd in hoofdletters
                    Relationname = worksheet.Cells["B" + rowNum].Text,
                    PurchaseItemcode = worksheet.Cells["C" + rowNum].Text.ToUpper(), //Ook alleen hoofdletters
                    PurchaseDescription = worksheet.Cells["D" + rowNum].Text,
                    Itemgroup = worksheet.Cells["E" + rowNum].Text,
                    Itemunit = worksheet.Cells["F" + rowNum].Text,
                };

                double.TryParse(worksheet.Cells["G" + rowNum].Text, out double grossPrice);
                row.Grossprice = grossPrice;

                double.TryParse(worksheet.Cells["H" + rowNum].Text, out double discount);
                row.Discount = discount;

                double.TryParse(worksheet.Cells["I" + rowNum].Text, out double netPrice);
                row.Netprice = netPrice;

                double.TryParse(worksheet.Cells["J" + rowNum].Text, out double salesMarkup);
                row.SalesMarkup = salesMarkup;

                rows.Add(row);
            }

            //Check maximum aan codes
            if(rows.Where(x => x.Itemcode.Length > 50 && x.Itemgroup != "" && x.Itemgroup != "0").Any())
            {
                List<string> codes = rows.Where(x => x.Itemcode.Length > 50 && x.Itemgroup != "" && x.Itemgroup != "0").Select(x => x.Itemcode).ToList();
                MessageBox.Show(
                    $"Een artikelcode mag maximaal 50 tekens zijn. Bij de volgende codes worden meer dan 50 tekens gebruikt: {string.Join(",", codes)}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                Invoke(new MethodInvoker(delegate
                {
                    progressBar1.Style = ProgressBarStyle.Blocks;
                    backgroundWorker1.ReportProgress(0);
                }));
                return;
            }

            if (rows.Where(x => x.Itemcode.Length > 10 && (x.Itemgroup == "" || x.Itemgroup == "0")).Any())
            {
                List<string> codes = rows.Where(x => x.Itemcode.Length > 10 && (x.Itemgroup == "" || x.Itemgroup == "0")).Select(x => x.Itemcode).ToList();
                MessageBox.Show(
                    $"Een uitbesteed werk code mag maximaal 10 tekens zijn. Bij de volgende codes worden meer dan 10 tekens gebruikt: {string.Join(",", codes)}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                Invoke(new MethodInvoker(delegate
                {
                    progressBar1.Style = ProgressBarStyle.Blocks;
                    backgroundWorker1.ReportProgress(0);
                }));

                return;
            }


            //2. - Haal de geselecteerde bedrijven op
            for (int i = 0; i < selectedCompanies.Count; i++)
            {
                var configuration = new SdkConfiguration
                {
                    CompanyName = selectedCompanies[i],
                    UserName = _session.Configuration.UserName,
                    Password = _session.Configuration.Password,
                    PersistSession = true,
                    RidderIQClientPath = _session.Configuration.RidderIQClientPath,
                };
                
                //backgroundWorker1.ReportProgress((int)Math.Round((Convert.ToDouble(i+2) / numberOfSteps)*100, 0));
                Invoke(new MethodInvoker(delegate
                {
                    label2.Text = string.Format("Inloggen {0}", selectedCompanies[i]);
                }));

                var newSession = new SdkSession(configuration);
                try
                {
                    newSession.Login();
                }
                catch
                {
                    MessageBox.Show(
                        $"Het is niet gelukt om in te loggen in bedrijf {selectedCompanies[i]}, controleer het SDK_ADMIN-wachtwoord.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    continue;
                }

                Invoke(new MethodInvoker(delegate
                {
                    label2.Text = "Ingelogd. Verzamelen crediteur-informatie.";
                }));

                var distinctSuppliers = rows.Where(x => x.Relationname != "").Select(x => x.Relationname).Distinct();

                Invoke(new MethodInvoker(delegate
                {
                    label2.Text = $"{distinctSuppliers.Count()} unieke leveranciers gevonden in Excel-bestand.";
                }));

                var rsSuppliers = newSession.GetRecordsetColumns("R_RELATION", "PK_R_RELATION, NAME",
                    $"NAME IN ('{string.Join("','", distinctSuppliers)}')");
                var suppliers = rsSuppliers.DataTable.AsEnumerable();

                Invoke(new MethodInvoker(delegate
                {
                    label2.Text =
                        $"{rsSuppliers.RecordCount} leveranciers gevonden in Ridder.";
                }));

                if (rsSuppliers.RecordCount == 0)
                {
                    MessageBox.Show(
                        $"Geen relaties gevonden met de naam: {string.Join(", ", distinctSuppliers)}. De import is overgeslagen.",
                        "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    continue;
                }

                var rsCreditors = newSession.GetRecordsetColumns("R_CREDITOR", "FK_RELATION",
                    $"FK_RELATION IN ({string.Join(",", suppliers.Select(x => x.Field<int>("PK_R_RELATION")))})", "");

                Invoke(new MethodInvoker(delegate
                {
                    label2.Text =
                        $"{rsSuppliers.RecordCount} leveranciers gevonden in Ridder; {rsCreditors.RecordCount} daarvan zijn crediteur.";
                }));

                if (distinctSuppliers.Count() != rsSuppliers.RecordCount)
                {
                    var notExistedSuppliers = distinctSuppliers.Except(suppliers.Select(x => x.Field<string>("NAME")));

                    MessageBox.Show(
                        $"Leverancier {string.Join(";", notExistedSuppliers)} is niet gevonden in administratie {selectedCompanies[i]}. De import is overgeslagen.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if(rsCreditors.RecordCount != rsSuppliers.RecordCount)
                {
                    List<int> relationIdFromCreditors = rsCreditors.DataTable.AsEnumerable().Select(x => x.Field<int>("FK_RELATION")).ToList();
                    var relationNotCreditor = suppliers.Where(x => !relationIdFromCreditors.Contains(x.Field<int>("PK_R_RELATION"))).Select(x => x.Field<string>("NAME"));

                    MessageBox.Show(
                        $"Relatie {string.Join(";", relationNotCreditor)} is geen leverancier in administratie {selectedCompanies[i]}. De import is overgeslagen.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    //Process item rows
                    var rowsItems = rows.Where(x => x.Itemcode != "").Where(x => x.Itemgroup != "" && x.Itemgroup != "0").ToList();

                    if (rowsItems.Count > 0)
                    {
                        ProcessItemPurchasePrices(newSession, rowsItems, suppliers, selectedCompanies[i]);
                    }
                    
                    //Proces outsourced activity rows
                    var rowsOutsourcedActivity = rows.Where(x => x.Itemcode != "").Where(x => x.Itemgroup == "" || x.Itemgroup == "0").ToList();

                    if (rowsOutsourcedActivity.Count > 0)
                    {
                        ProcessOutsourcedActivityPurchasePrices(newSession, rowsOutsourcedActivity, suppliers, selectedCompanies[i]);
                    }

                }
            }

            
            Invoke(new MethodInvoker(delegate
            {
                progressBar1.Style = ProgressBarStyle.Blocks;
                backgroundWorker1.ReportProgress(100);
                label2.Text = "Import voltooid";
            }));
            
        }

 

        private void ProcessItemPurchasePrices(SdkSession newSession, List<ExcelRow> rowsItems, EnumerableRowCollection<DataRow> suppliers, string companyName)
        {
            if(!rowsItems.Any())
            {
                return;
            }

            Invoke(new MethodInvoker(delegate
            {
                label2.Text = $"Start verwerking artikel inkoopprijzen.";
            }));

            var distinctUnits = rowsItems.Where(x => x.Itemunit != "").Select(x => x.Itemunit).Distinct();
            var distinctGroups = rowsItems.Where(x => x.Itemunit != "").Select(x => x.Itemgroup).Distinct();

            //Bepaal itemunits en itemgroups
            var rsItemUnits = newSession.GetRecordsetColumns("R_ITEMUNIT", "PK_R_ITEMUNIT, CODE",
                $"CODE IN ('{string.Join("','", distinctUnits)}')");
            var itemunits = rsItemUnits.DataTable.AsEnumerable().ToList();

            if (distinctUnits.Count() != rsItemUnits.RecordCount)
            {
                var notExistedUnits = distinctUnits.Except(itemunits.Select(x => x.Field<string>("CODE")));

                MessageBox.Show(
                    $"De artikeleenheid {string.Join(";", notExistedUnits)} is niet gevonden in administratie {companyName}. De import van artikelprijzen is overgeslagen.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var rsItemGroups = newSession.GetRecordsetColumns("R_ITEMGROUP", "PK_R_ITEMGROUP, CODE",
                $"CODE IN ('{string.Join("','", distinctGroups)}')");
            var itemgroups = rsItemGroups.DataTable.AsEnumerable();

            if (distinctGroups.Count() != rsItemGroups.RecordCount)
            {
                var notExistedGroups = distinctGroups.Except(itemgroups.Select(x => x.Field<string>("CODE")));

                MessageBox.Show(
                    $"De artikelgroep {string.Join(";", notExistedGroups)} is niet gevonden in administratie {companyName}. De import van artikelprijzen is overgeslagen.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            List<Item> items = new List<Item>();
            List<string> existingItemCodes = new List<string>();
            var itemcodes = rowsItems.Select(x => x.Itemcode.ToUpper().Trim()).ToList();
            foreach (var batch in itemcodes.Batch(2000))
            {
                var rsItems = newSession.GetRecordsetColumns("R_ITEM", "PK_R_ITEM, CODE",
                    $"CODE IN ('{string.Join("','", batch)}')");

                items.AddRange(rsItems.DataTable.AsEnumerable().Select(x => new Item()
                {
                    ItemId = x.Field<int>("PK_R_ITEM"),
                    Code = x.Field<string>("CODE")
                        .ToUpper() //Code is alleen hoofdletters. Voor de zekerheid alleen hoofdletters vergelijken met hoofdletters. 
                }).ToList());

                existingItemCodes.AddRange(items.Select(x => x.Code).Distinct().ToList());
            }


            var toCreateItemCodes = rowsItems.Where(x => x.Itemcode != "" && !existingItemCodes.Contains(x.Itemcode.ToUpper().Trim()))
                .Select(x => x.Itemcode.ToUpper().Trim()).Distinct().ToList();

            if (toCreateItemCodes.Any())
            {
                Invoke(new MethodInvoker(delegate
                {
                    label2.Text = $"{toCreateItemCodes.Count()} nieuwe artikelen gevonden.";
                }));

                var rsItemNew = newSession.GetRecordsetColumns("R_ITEM", "", "PK_R_ITEM <= -1");
                rsItemNew.UseDataChanges = true;
                rsItemNew.UpdateWhenMoveRecord = false;

                try
                {
                    int purchasePriceTemplate = DetermineDefaultPurchasePriceTemplate(newSession);
                    int salesPriceTemplate = DetermineDefaultSalesPriceTemplate(newSession);

                    if(purchasePriceTemplate == 0 || salesPriceTemplate == 0)
                    {
                        return;
                    }

                    var dummy = 0;

                    foreach (var tci in toCreateItemCodes)
                    {
                        var itemInfo = rowsItems.FirstOrDefault(x => x.Itemcode == tci);

                        if(itemInfo == null)
                        {
                            MessageBox.Show($"Het aanmaken van artikelen is mislukt, ophalen itemInfo '{tci}' is mislukt.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }

                        var itemUnitId = itemunits.FirstOrDefault(x => x.Field<string>("CODE").Equals(itemInfo.Itemunit, StringComparison.OrdinalIgnoreCase))?.Field<int>("PK_R_ITEMUNIT") ?? 0;

                        if(itemUnitId == 0)
                        {
                            MessageBox.Show($"Het aanmaken van artikelen is mislukt, geen item unit id gevonden voor '{tci}' met code '{itemInfo.Itemunit}'.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }

                        var itemGroupId = itemgroups.FirstOrDefault(x => x.Field<string>("CODE").Equals(itemInfo.Itemgroup, StringComparison.OrdinalIgnoreCase))?.Field<int>("PK_R_ITEMGROUP") ?? 0;

                        if (itemGroupId == 0)
                        {
                            MessageBox.Show($"Het aanmaken van artikelen is mislukt, geen item group id gevonden voor '{tci}' met code '{itemInfo.Itemgroup}'.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }

                        rsItemNew.AddNew();
                        rsItemNew.Fields["CODE"].Value = tci;
                        rsItemNew.Fields["DESCRIPTION"].Value = itemInfo.PurchaseDescription;

                        rsItemNew.Fields["FK_ITEMUNIT"].Value = itemUnitId;
                        rsItemNew.Fields["FK_ITEMGROUP"].Value = itemGroupId;
                        
                        rsItemNew.Fields["REGISTRATIONPATH"].Value = 1; //Zelf OHW-boeken

                        rsItemNew.Fields["FK_ITEMPURCHASEPRICETEMPLATEGROUP"].Value = purchasePriceTemplate;
                        rsItemNew.Fields["FK_ITEMSALESPRICETEMPLATEGROUP"].Value = salesPriceTemplate;

                        Invoke(new MethodInvoker(delegate
                        {
                            label2.Text = $"Nieuwe artikelen aanmaken {dummy} / {toCreateItemCodes.Count()}";
                        }));
                    }

                    rsItemNew.MoveFirst();
                    var result = rsItemNew.Update2();

                    Invoke(new MethodInvoker(delegate
                    {
                        label2.Text = $"Aanmaken {toCreateItemCodes.Count()} artikelen in Ridder iQ.";
                    }));

                    if (result != null && result.Where(x => x.HasError).Count() > 0)
                    {
                        MessageBox.Show(
                            $"Het aanmaken van artikelen is mislukt, oorzaak: {string.Join(";", result.Select(x => x.GetResult()))}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(
                        $"Het aanmaken van artikelen is mislukt, oorzaak: {ex.Message}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                Invoke(new MethodInvoker(delegate
                {
                    label2.Text = $"{toCreateItemCodes.Count()} artikelen aangemaakt.";
                }));

                List<Item> createdItems = rsItemNew.DataTable.AsEnumerable().Select(x => new Item()
                {
                    ItemId = x.Field<int>("PK_R_ITEM"),
                    Code = x.Field<string>("CODE")
                }).ToList();

                items = items.Concat(createdItems).ToList();
            }

            //Controleer welke itemsuppliers nog niet bestaan en aangemaakt of aangepast moeten worden
            var ItemSupplierExcelInfo =
                from rowItem in rowsItems
                join item in items on rowItem.Itemcode.ToUpper().Trim() equals item.Code.ToUpper() //Vermijd hoofdletter gevoelig
                join supplier in suppliers on rowItem.Relationname equals supplier.Field<string>("NAME")
                select new
                {
                    ItemCode = rowItem.Itemcode.ToUpper(),
                    ItemId = item.ItemId,
                    RelationName = rowItem.Relationname,
                    RelationId = supplier.Field<int>("PK_R_RELATION"),
                    PurchaseItemCode = rowItem.PurchaseItemcode,
                    PurchaseDescription = rowItem.PurchaseDescription,
                    GrossPurchasePrice = rowItem.Grossprice,
                    DiscountPercentage = rowItem.Discount,
                };

            List<ItemSupplier> ItemSuppliersFromExcelFile = ItemSupplierExcelInfo.Select(x => new ItemSupplier()
            {
                ItemSupplierId = 0,
                SupplierId = x.RelationId,
                ItemId = x.ItemId,
                PurchaseItemCode = x.PurchaseItemCode,
                PurchaseDescription = x.PurchaseDescription,
                TempPk = $"{x.RelationId}_{x.ItemId}",
            }).ToList();

            //Haal de bestaande itemsuppliers op
            var rsItemSupplier = newSession.GetRecordsetColumns("R_ITEMSUPPLIER", "PK_R_ITEMSUPPLIER, FK_RELATION, FK_ITEM, PURCHASEITEMCODE, PURCHASEDESCRIPTION",
                $"FK_RELATION IN ({string.Join(",", suppliers.Select(x => x.Field<int>("PK_R_RELATION")))}) AND FK_ITEM IN ({string.Join(",", items.Select(x => x.ItemId))})");
            List<ItemSupplier> existingItemSuppliers = rsItemSupplier.DataTable.AsEnumerable().Select(x => new ItemSupplier()
            {
                ItemSupplierId = x.Field<int>("PK_R_ITEMSUPPLIER"),
                SupplierId = x.Field<int>("FK_RELATION"),
                ItemId = x.Field<int>("FK_ITEM"),
                PurchaseItemCode = x.Field<string>("PURCHASEITEMCODE"),
                PurchaseDescription = x.Field<string>("PURCHASEDESCRIPTION"),
                TempPk = $"{x.Field<int>("FK_RELATION")}_{x.Field<int>("FK_ITEM")}",
            }).ToList();

            //Hier voegen we later de nieuw aangemaakte itemsuppliers bij
            List<ItemSupplier> allItemSuppliers = existingItemSuppliers;

            var ItemSuppliersToUpdate =
            (
                from ItemSupplierFromExcelFile in ItemSuppliersFromExcelFile
                join existingItemSupplier in existingItemSuppliers on ItemSupplierFromExcelFile.TempPk equals existingItemSupplier.TempPk
                select new
                {
                    ItemSupplierId = existingItemSupplier.ItemSupplierId,
                    TempPk = ItemSupplierFromExcelFile.TempPk,
                    PurchaseItemCodeExcel = ItemSupplierFromExcelFile.PurchaseItemCode,
                    PurchaseItemCodeRidder = existingItemSupplier.PurchaseItemCode,
                    PurchaseDescriptionExcel = ItemSupplierFromExcelFile.PurchaseDescription,
                    PurchaseDescriptionRidder = existingItemSupplier.PurchaseDescription,
                }).Where(x => x.PurchaseItemCodeExcel.Trim() != x.PurchaseItemCodeRidder.Trim() || x.PurchaseDescriptionExcel.Trim() != x.PurchaseDescriptionRidder.Trim());

            if (ItemSuppliersToUpdate.Any())
            {
                Invoke(new MethodInvoker(delegate
                {
                    label2.Text = $"{ItemSuppliersToUpdate.Count()} artikelen per leverancier bij te werken.";
                }));

                var rsItemSupplierToUpdate = newSession.GetRecordsetColumns("R_ITEMSUPPLIER", "PK_R_ITEMSUPPLIER, FK_RELATION, FK_ITEM, PURCHASEITEMCODE, PURCHASEDESCRIPTION",
                    $"PK_R_ITEMSUPPLIER IN ({string.Join(",", ItemSuppliersToUpdate.Select(x => x.ItemSupplierId))})");
                rsItemSupplierToUpdate.UseDataChanges = false;
                rsItemSupplierToUpdate.UpdateWhenMoveRecord = false;
                rsItemSupplierToUpdate.MoveFirst();

                var dummy = 0;

                while (!rsItemSupplierToUpdate.EOF)
                {
                    var itemSupplierToUpdateInfo = ItemSuppliersToUpdate.First(x => x.ItemSupplierId == (int)rsItemSupplierToUpdate.GetField("PK_R_ITEMSUPPLIER").Value);

                    rsItemSupplierToUpdate.SetFieldValue("PURCHASEITEMCODE", itemSupplierToUpdateInfo.PurchaseItemCodeExcel);
                    rsItemSupplierToUpdate.SetFieldValue("PURCHASEDESCRIPTION", itemSupplierToUpdateInfo.PurchaseDescriptionExcel);

                    dummy++;
                    Invoke(new MethodInvoker(delegate
                    {
                        label2.Text = $"Bijwerken artikelen per leverancier {dummy} / {ItemSuppliersToUpdate.Count()}";
                    }));

                    rsItemSupplierToUpdate.MoveNext();
                }

                Invoke(new MethodInvoker(delegate
                {
                    label2.Text = $"Opslaan {ItemSuppliersToUpdate.Count()} artikelen per leverancier...";
                }));

                rsItemSupplierToUpdate.MoveFirst();
                var resultUpdateItemSuppliers = rsItemSupplierToUpdate.Update2();

                Invoke(new MethodInvoker(delegate
                {
                    label2.Text = $"{ItemSuppliersToUpdate.Count()} artikelen per leverancier opgeslagen";
                }));

                if (resultUpdateItemSuppliers != null && resultUpdateItemSuppliers.Any(x => x.HasError))
                {
                    MessageBox.Show(
                        $"Het bijwerken van artikelen per leverancier is mislukt, oorzaak: {string.Join(";", resultUpdateItemSuppliers.First(x => x.HasError).GetResult())}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }

            //Maak nieuwe itemsuppliers aan
            List<string> existingItemSupplierCombinations = existingItemSuppliers.Select(x => x.TempPk).ToList();
            List<ItemSupplier> toCreateItemSuppliers = ItemSuppliersFromExcelFile.Where(x => !existingItemSupplierCombinations.Contains(x.TempPk)).ToList();

            Invoke(new MethodInvoker(delegate
            {
                label2.Text = $"{toCreateItemSuppliers.Count()} nieuwe artikelen per leverancier gevonden.";
            }));

            if (toCreateItemSuppliers.Any())
            {
                var rsItemSupplierNew = newSession.GetRecordsetColumns("R_ITEMSUPPLIER", "", "PK_R_ITEMSUPPLIER <= -1", "");
                rsItemSupplierNew.UseDataChanges = true;
                rsItemSupplierNew.UpdateWhenMoveRecord = false;

                var dummy = 0;

                foreach (var itemSupplier in toCreateItemSuppliers)
                {
                    rsItemSupplierNew.AddNew();
                    rsItemSupplierNew.Fields["FK_ITEM"].Value = itemSupplier.ItemId;
                    rsItemSupplierNew.Fields["FK_RELATION"].Value = itemSupplier.SupplierId;
                    rsItemSupplierNew.Fields["PURCHASEITEMCODE"].Value = itemSupplier.PurchaseItemCode;
                    rsItemSupplierNew.Fields["PURCHASEDESCRIPTION"].Value = itemSupplier.PurchaseDescription;

                    dummy++;
                    Invoke(new MethodInvoker(delegate
                    {
                        label2.Text = $"Aanmaken artikelen per leverancier {dummy} / {toCreateItemSuppliers.Count()}";
                    }));
                }

                Invoke(new MethodInvoker(delegate
                {
                    label2.Text = $"{toCreateItemSuppliers.Count()} nieuwe artikelen per leverancier aanmaken...";
                }));

                rsItemSupplierNew.MoveFirst();
                var resultInsertItemSuppliers = rsItemSupplierNew.Update2();

                Invoke(new MethodInvoker(delegate
                {
                    label2.Text = $"{toCreateItemSuppliers.Count()} nieuwe artikelen per leverancier aangemaakt.";
                }));

                if (resultInsertItemSuppliers != null && resultInsertItemSuppliers.Any(x => x.HasError))
                {
                    MessageBox.Show(
                        $"Het aanmaken van artikelen per leverancier is mislukt, oorzaak: {string.Join(";", resultInsertItemSuppliers.First(x => x.HasError).GetResult())}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                List<ItemSupplier> createdItemSuppliers = rsItemSupplierNew.DataTable.AsEnumerable().Select(x => new ItemSupplier()
                {
                    ItemSupplierId = x.Field<int>("PK_R_ITEMSUPPLIER"),
                    SupplierId = x.Field<int>("FK_RELATION"),
                    ItemId = x.Field<int>("FK_ITEM"),
                    PurchaseItemCode = x.Field<string>("PURCHASEITEMCODE"),
                    PurchaseDescription = x.Field<string>("PURCHASEDESCRIPTION"),
                    TempPk = $"{x.Field<int>("FK_RELATION")}_{x.Field<int>("FK_ITEM")}",
                }).ToList();

                allItemSuppliers = allItemSuppliers.Concat(createdItemSuppliers).ToList();
            }

            //Verwerk de basisprijs en het kortingspercentage naar de tabel 'R_ITEMPURCHASEPRICE'. 
            //Loop de aangemaakte itemsupplier door en zorg dat de inkoopprijs hierbij ingevuld wordt. Haal alleen de onderdelen 'Basisprijs' en 'Korting' op van de inkoopprijzen. Niet de kortingsstaffel 'Niet verstek'.
            Invoke(new MethodInvoker(delegate
            {
                label2.Text = $"{allItemSuppliers.Count} nieuwe inkoopprijzen gevonden.";
            }));

            var basisprice = newSession.GetRecordsetColumns("R_PRICENAME", "PK_R_PRICENAME", "NAME = 'Basisprijs'")
                .DataTable.AsEnumerable().FirstOrDefault();

            if(basisprice == null)
            {
                Invoke(new MethodInvoker(delegate
                {
                    label2.Text = $"Prijzen bijwerken is mislukt. Geen prijsnaam 'Basisprijs' gevonden.";
                }));

                return;
            }


            var rsItemSupplierPurchasePriceIds = newSession.GetRecordsetColumns("R_ITEMPURCHASEPRICE",
                "PK_R_ITEMPURCHASEPRICE",
                $"FK_ITEMSUPPLIER IN ({string.Join(",", allItemSuppliers.Select(x => x.ItemSupplierId))}) AND ((VALUETYPE = 1 AND FK_PRICENAME = '{basisprice.Field<Guid>("PK_R_PRICENAME")}') OR VALUETYPE = 2)",
                "");

            var foundItemSupplierPurchasePriceIds = rsItemSupplierPurchasePriceIds.DataTable.AsEnumerable()
                .Select(x => x.Field<int>("PK_R_ITEMPURCHASEPRICE")).ToList();

            var batchNumber = 0;
            foreach (var batch in foundItemSupplierPurchasePriceIds.Batch(2000))
            {
                batchNumber++;
                Invoke(new MethodInvoker(delegate
                {
                    label2.Text = $"Start verwerking batch {batchNumber}.";
                }));

                var rsItemSupplierPurchasePrices = newSession.GetRecordsetColumns("R_ITEMPURCHASEPRICE",
                    "FK_ITEMSUPPLIER, VALUE, VALUETYPE",
                    $"PK_R_ITEMPURCHASEPRICE IN ({string.Join(",", batch)})",
                    "FK_ITEMSUPPLIER ASC");

                rsItemSupplierPurchasePrices.MoveFirst();
                rsItemSupplierPurchasePrices.UseDataChanges = false;
                rsItemSupplierPurchasePrices.UpdateWhenMoveRecord = false;

                var numberOfBatch = rsItemSupplierPurchasePrices.DataTable.AsEnumerable()
                    .Select(x => x.Field<int>("FK_ITEMSUPPLIER")).Distinct().Count();
                List<int> processItemSuppliers = new List<int>();

                while (!rsItemSupplierPurchasePrices.EOF)
                {
                    var itemSupplierInfo = allItemSuppliers.First(x => x.ItemSupplierId == (int)rsItemSupplierPurchasePrices.Fields["FK_ITEMSUPPLIER"].Value);
                    var ExcelPriceInfo = ItemSupplierExcelInfo.First(x => x.RelationId == itemSupplierInfo.SupplierId && x.ItemId == itemSupplierInfo.ItemId);

                    if ((int)rsItemSupplierPurchasePrices.Fields["VALUETYPE"].Value == 1) //Bedrag
                    {
                        rsItemSupplierPurchasePrices.Fields["VALUE"].Value = ExcelPriceInfo.GrossPurchasePrice;
                    }
                    else
                    {
                        //Value = 2 = Percentage
                        rsItemSupplierPurchasePrices.Fields["VALUE"].Value = ExcelPriceInfo.DiscountPercentage;
                    }

                    if (!processItemSuppliers.Contains(itemSupplierInfo.ItemSupplierId))
                    {
                        processItemSuppliers.Add(itemSupplierInfo.ItemSupplierId);
                    }

                    Invoke(new MethodInvoker(delegate
                    {
                        label2.Text = $"Batch {batchNumber}: Inkoopprijzen bijwerken: {processItemSuppliers.Count} / {numberOfBatch} ({allItemSuppliers.Count} totaal nieuwe prijzen)";
                    }));

                    rsItemSupplierPurchasePrices.MoveNext();
                }

                Invoke(new MethodInvoker(delegate
                {
                    label2.Text = $"Batch {batchNumber}: {numberOfBatch} inkoopprijzen opslaan...";
                }));

                rsItemSupplierPurchasePrices.MoveFirst();
                var result3 = rsItemSupplierPurchasePrices.Update2();

                if (result3 != null && result3.Where(x => x.HasError).Count() > 0)
                {
                    MessageBox.Show(
                        $"Het invullen van de artikel prijzen is mislukt, oorzaak: {string.Join(";", result3.Select(x => x.GetResult()))}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                Invoke(new MethodInvoker(delegate
                {
                    label2.Text = $"Batch {batchNumber}: {numberOfBatch} inkoopprijzen opgeslagen.";
                }));
            }
        }

        private int DetermineDefaultPurchasePriceTemplate(SdkSession session)
        {
            var rsPurchasePriceTemplate = session.GetRecordsetColumns("R_ITEMPURCHASEPRICETEMPLATEGROUP", "PK_R_ITEMPURCHASEPRICETEMPLATEGROUP", "CODE = 'INKOOP'");

            if(rsPurchasePriceTemplate.RecordCount == 0)
            {
                MessageBox.Show(
                    $"Geen prijssjabloon 'INKOOP' gevonden.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 0;
            }

            rsPurchasePriceTemplate.MoveFirst();

            return (rsPurchasePriceTemplate.RecordCount == 0) ? 1 : (int)rsPurchasePriceTemplate.Fields["PK_R_ITEMPURCHASEPRICETEMPLATEGROUP"].Value;
        }

        private int DetermineDefaultSalesPriceTemplate(SdkSession session)
        {
            var rsSalesPriceTemplate = session.GetRecordsetColumns("R_ITEMSALESPRICETEMPLATEGROUP", "PK_R_ITEMSALESPRICETEMPLATEGROUP", "CODE = 'VERKOOP'");

            if (rsSalesPriceTemplate.RecordCount == 0)
            {
                MessageBox.Show(
                    $"Geen prijssjabloon 'VERKOOP' gevonden.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 0;
            }


            rsSalesPriceTemplate.MoveFirst();

            return (rsSalesPriceTemplate.RecordCount == 0) ? 1 : (int)rsSalesPriceTemplate.Fields["PK_R_ITEMSALESPRICETEMPLATEGROUP"].Value;
        }

        private void ProcessOutsourcedActivityPurchasePrices(SdkSession newSession, List<ExcelRow> rowsOutsourcedActivity, EnumerableRowCollection<DataRow> suppliers, string companyName)
        {
            if(!rowsOutsourcedActivity.Any())
            {
                return;
            }

            Invoke(new MethodInvoker(delegate
            {
                label2.Text = $"Start verwerking uitbesteed werk inkoopprijzen.";
            }));

            var distinctUnits = rowsOutsourcedActivity.Where(x => x.Itemunit != "").Select(x => x.Itemunit).Distinct();

            //Bepaal units
            var rsOutsourcedUnits = newSession.GetRecordsetColumns("R_UNIT", "PK_R_UNIT, CODE",
                $"CODE IN ('{string.Join("','", distinctUnits)}')");
            var outsourcedUnits = rsOutsourcedUnits.DataTable.AsEnumerable();

            if (distinctUnits.Count() != rsOutsourcedUnits.RecordCount)
            {
                var notExistedUnits = distinctUnits.Except(outsourcedUnits.Select(x => x.Field<string>("CODE")));

                MessageBox.Show(
                    $"De uitbesteed werk eenheid {string.Join(";", notExistedUnits)} is niet gevonden in administratie {companyName}. De import van uitbesteed werk inkoopprijzen is overgeslagen.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            
            var rsOutsourcedActivities = newSession.GetRecordsetColumns("R_OUTSOURCEDACTIVITY", "PK_R_OUTSOURCEDACTIVITY, CODE, SALESMARKUP",
                $"CODE IN ('{string.Join("','", rowsOutsourcedActivity.Select(x => x.Itemcode))}')");
            List<OutsourcedActivity> outsourcedActivities = rsOutsourcedActivities.DataTable.AsEnumerable().Select(x => new OutsourcedActivity()
            {
                OutsourcedActivityId = x.Field<int>("PK_R_OUTSOURCEDACTIVITY"),
                Code = x.Field<string>("CODE").ToUpper(), //Code is alleen hoofdletters. Voor de zekerheid alleen hoofdletters vergelijken met hoofdletters.
                SalesMarkup = x.Field<double>("SALESMARKUP"),
            }).ToList();
            List<string> existingCodes = outsourcedActivities.Select(x => x.Code).Distinct().ToList();

            var toCreateOutsourcedActivityCodes = rowsOutsourcedActivity
                .Where(x => x.Itemcode != "" && !existingCodes.Contains(x.Itemcode)).Select(x => x.Itemcode).Distinct();


            if (toCreateOutsourcedActivityCodes.Any())
            {
                Invoke(new MethodInvoker(delegate
                {
                    label2.Text = $"{toCreateOutsourcedActivityCodes.Count()} nieuwe uitbesteed werk gevonden om aan te maken.";
                }));

                var rsOutsourcedActivityNew = newSession.GetRecordsetColumns("R_OUTSOURCEDACTIVITY", "", "PK_R_OUTSOURCEDACTIVITY <= -1");
                rsOutsourcedActivityNew.UseDataChanges = true;
                rsOutsourcedActivityNew.UpdateWhenMoveRecord = false;

                var dummy = 0;

                foreach (var tcoa in toCreateOutsourcedActivityCodes)
                {
                    var outsourcedActivityInfo = rowsOutsourcedActivity.First(x => x.Itemcode == tcoa);

                    rsOutsourcedActivityNew.AddNew();

                    rsOutsourcedActivityNew.Fields["CODE"].Value = tcoa;
                    rsOutsourcedActivityNew.Fields["DESCRIPTION"].Value = outsourcedActivityInfo.PurchaseDescription;
                    string unitcode = outsourcedActivityInfo.Itemunit;
                    rsOutsourcedActivityNew.Fields["FK_UNIT"].Value = outsourcedUnits.Where(x => x.Field<string>("CODE") == unitcode).Select(x => x.Field<int>("PK_R_UNIT")).First();
                    rsOutsourcedActivityNew.Fields["REGISTRATIONPATH"].Value = 1; //Zelf OHW-boeken

                    //Indien het ubw aangemaakt moet worden, neem dan de verkoopopslag direct mee
                    rsOutsourcedActivityNew.Fields["SALESMARKUP"].Value = outsourcedActivityInfo.SalesMarkup;

                    dummy++;
                    Invoke(new MethodInvoker(delegate
                    {
                        label2.Text = $"Aanmaken uitbesteed werk {dummy} / {toCreateOutsourcedActivityCodes.Count()}";
                    }));
                }

                Invoke(new MethodInvoker(delegate
                {
                    label2.Text = $"Aanmaken {toCreateOutsourcedActivityCodes.Count()} nieuwe uitbesteed werk...";
                }));

                rsOutsourcedActivityNew.MoveFirst();
                var result = rsOutsourcedActivityNew.Update2();

                Invoke(new MethodInvoker(delegate
                {
                    label2.Text = $"{toCreateOutsourcedActivityCodes.Count()} nieuwe uitbesteed werk aangemaakt.";
                }));

                if (result != null && result.Any(x => x.HasError))
                {
                    MessageBox.Show(string.Format("Het aanmaken van nieuwe uitbesteed werk is mislukt, oorzaak: {0}", string.Join(";", result.First(x => x.HasError).GetResult()), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error));
                    return;
                }

                List<OutsourcedActivity> createdOutsourcedActivities = rsOutsourcedActivityNew.DataTable.AsEnumerable().Select(x => new OutsourcedActivity()
                {
                    OutsourcedActivityId = x.Field<int>("PK_R_OUTSOURCEDACTIVITY"),
                    Code = x.Field<string>("CODE"), 
                    SalesMarkup = x.Field<double>("SALESMARKUP")
                }).ToList();

                outsourcedActivities = outsourcedActivities.Concat(createdOutsourcedActivities).ToList();
            }

            //Controleer welke outsourced suppliers nog niet bestaan en aangemaakt of aangepast moeten worden
            var OutsourcedSupplierExcelInfo =
                from rowOutsourcedActivity in rowsOutsourcedActivity
                join outsourcedActivity in outsourcedActivities on rowOutsourcedActivity.Itemcode equals outsourcedActivity.Code
                join supplier in suppliers on rowOutsourcedActivity.Relationname equals supplier.Field<string>("NAME")
                select new
                {
                    ItemCode = rowOutsourcedActivity.Itemcode,
                    OutSourcedActivityId = outsourcedActivity.OutsourcedActivityId,
                    RelationName = rowOutsourcedActivity.Relationname,
                    RelationId = supplier.Field<int>("PK_R_RELATION"),
                    Rate = rowOutsourcedActivity.Netprice,
                    SalesMarkup = rowOutsourcedActivity.SalesMarkup
                };
            
            List<OutsourcedSupplier> OutsourcedSuppliersFromExcelFile = OutsourcedSupplierExcelInfo.Select(x => new OutsourcedSupplier()
            {
                OutsourcedSupplierId = 0,
                SupplierId = x.RelationId,
                OutsourcedActivityId = x.OutSourcedActivityId,
                MainSupplier = false,
                Rate = x.Rate,
                TempPk = $"{x.RelationId}_{x.OutSourcedActivityId}",
            }).ToList();

            //Haal de bestaande outsourced suppliers op
            var rsOutsourcedSupplier = newSession.GetRecordsetColumns("R_OUTSOURCEDPURCHASEPRICE", "PK_R_OUTSOURCEDPURCHASEPRICE, FK_SUPPLIER, FK_OUTSOURCEDACTIVITY, MAINSUPPLIER, RATE",
                $"FK_SUPPLIER IN ({string.Join(",", suppliers.Select(x => x.Field<int>("PK_R_RELATION")))}) AND FK_OUTSOURCEDACTIVITY IN ({string.Join(",", outsourcedActivities.Select(x => x.OutsourcedActivityId))})");
            List<OutsourcedSupplier> existingOutsourcedSuppliers = rsOutsourcedSupplier.DataTable.AsEnumerable().Select(x => new OutsourcedSupplier()
            {
                OutsourcedSupplierId = x.Field<int>("PK_R_OUTSOURCEDPURCHASEPRICE"),
                SupplierId = x.Field<int>("FK_SUPPLIER"),
                OutsourcedActivityId = x.Field<int>("FK_OUTSOURCEDACTIVITY"),
                MainSupplier = x.Field<bool>("MAINSUPPLIER"),
                Rate = x.Field<double>("RATE"),
                TempPk = $"{x.Field<int>("FK_SUPPLIER")}_{x.Field<int>("FK_OUTSOURCEDACTIVITY")}",
            }).ToList();

            //Hier voegen we later de nieuw aangemaakte outsourced suppliers bij
            List<OutsourcedSupplier> allOutsourcedSuppliers = existingOutsourcedSuppliers;

            var outsourcedSuppliersToUpdate =
                (
                from OutsourcedSupplierFromExcelFile in OutsourcedSuppliersFromExcelFile
                join existingOutsourcedSupplier in existingOutsourcedSuppliers on OutsourcedSupplierFromExcelFile.TempPk equals existingOutsourcedSupplier.TempPk
                select new
                {
                    OutsourcedSupplierId = existingOutsourcedSupplier.OutsourcedSupplierId,
                    TempPk = OutsourcedSupplierFromExcelFile.TempPk,
                    RateExcel = OutsourcedSupplierFromExcelFile.Rate,
                    RateRidder = existingOutsourcedSupplier.Rate,
                }).Where(x => Math.Abs(x.RateExcel - x.RateRidder) > 0.01);

            if (outsourcedSuppliersToUpdate.Any())
            {
                Invoke(new MethodInvoker(delegate
                {
                    label2.Text = $"{outsourcedSuppliersToUpdate.Count()} uitbesteed werk inkoopprijs gevonden om bij te werken.";
                }));

                var rsOutsourcedSupplierToUpdate = newSession.GetRecordsetColumns("R_OUTSOURCEDPURCHASEPRICE",
                    "PK_R_OUTSOURCEDPURCHASEPRICE, FK_SUPPLIER, FK_OUTSOURCEDACTIVITY, MAINSUPPLIER, RATE",
                    $"PK_R_OUTSOURCEDPURCHASEPRICE IN ({string.Join(",", outsourcedSuppliersToUpdate.Select(x => x.OutsourcedSupplierId))})");
                rsOutsourcedSupplierToUpdate.UseDataChanges = false;
                rsOutsourcedSupplierToUpdate.UpdateWhenMoveRecord = false;
                rsOutsourcedSupplierToUpdate.MoveFirst();

                var dummy = 0;

                while (!rsOutsourcedSupplierToUpdate.EOF)
                {
                    var outsourcedSupplierToUpdateInfo = outsourcedSuppliersToUpdate.First(x => x.OutsourcedSupplierId == (int)rsOutsourcedSupplierToUpdate.GetField("PK_R_OUTSOURCEDPURCHASEPRICE").Value);

                    rsOutsourcedSupplierToUpdate.SetFieldValue("RATE", outsourcedSupplierToUpdateInfo.RateExcel);

                    Invoke(new MethodInvoker(delegate
                    {
                        label2.Text = $"Bijwerken uitbesteed werk inkoopprijzen {dummy} / {outsourcedSuppliersToUpdate.Count()}";
                    }));

                    rsOutsourcedSupplierToUpdate.MoveNext();
                }

                Invoke(new MethodInvoker(delegate
                {
                    label2.Text = $"Opslaan {outsourcedSuppliersToUpdate.Count()} uitbesteed werk inkoopprijzen...";
                }));

                rsOutsourcedSupplierToUpdate.MoveFirst();
                var resultUpdateOutsourcedSuppliers = rsOutsourcedSupplierToUpdate.Update2();

                Invoke(new MethodInvoker(delegate
                {
                    label2.Text = $"{outsourcedSuppliersToUpdate.Count()} uitbesteed werk inkoopprijzen opgeslagen.";
                }));

                if (resultUpdateOutsourcedSuppliers != null && resultUpdateOutsourcedSuppliers.Any(x => x.HasError))
                {
                    MessageBox.Show(
                        $"Het bijwerken van het tarief het uitbesteed werk is mislukt, oorzaak: {string.Join(";", resultUpdateOutsourcedSuppliers.First(x => x.HasError).GetResult())}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }

            //Maak nieuwe outsourced suppliers aan
            List<string> existingOutsourcedSupplierCombinations = existingOutsourcedSuppliers.Select(x => x.TempPk).ToList();
            List<OutsourcedSupplier> toCreateOutsourcedSuppliers = OutsourcedSuppliersFromExcelFile.Where(x => !existingOutsourcedSupplierCombinations.Contains(x.TempPk)).ToList();

            if (toCreateOutsourcedSuppliers.Any())
            {
                Invoke(new MethodInvoker(delegate
                {
                    label2.Text = $"{toCreateOutsourcedSuppliers.Count()} nieuwe uitbesteed werk inkoopprijzen gevonden.";
                }));

                var rsOutsourcedSuppliersIncludingOtherSuppliers = newSession.GetRecordsetColumns("R_OUTSOURCEDPURCHASEPRICE", "PK_R_OUTSOURCEDPURCHASEPRICE, FK_OUTSOURCEDACTIVITY",
                    $"FK_OUTSOURCEDACTIVITY IN ({string.Join(",", toCreateOutsourcedSuppliers.Select(x => x.OutsourcedActivityId))})", "");
                var outsourcedSuppliersIncludingOtherSuppliers = rsOutsourcedSuppliersIncludingOtherSuppliers.DataTable.AsEnumerable();

                var rsOutsourcedSupplierNew = newSession.GetRecordsetColumns("R_OUTSOURCEDPURCHASEPRICE", "", "PK_R_OUTSOURCEDPURCHASEPRICE <= -1", "");
                rsOutsourcedSupplierNew.UseDataChanges = true;
                rsOutsourcedSupplierNew.UpdateWhenMoveRecord = false;

                var dummy = 0;

                foreach (var outsourcedSupplier in toCreateOutsourcedSuppliers)
                {
                    rsOutsourcedSupplierNew.AddNew();
                    rsOutsourcedSupplierNew.Fields["FK_OUTSOURCEDACTIVITY"].Value = outsourcedSupplier.OutsourcedActivityId;
                    rsOutsourcedSupplierNew.Fields["FK_SUPPLIER"].Value = outsourcedSupplier.SupplierId;
                    rsOutsourcedSupplierNew.Fields["RATE"].Value = outsourcedSupplier.Rate;

                    //Indien dit de eerste inkoopprijs is, dan is dit de hoofdleverancier
                    if (!outsourcedSuppliersIncludingOtherSuppliers.Any(x => x.Field<int>("FK_OUTSOURCEDACTIVITY") == outsourcedSupplier.OutsourcedActivityId))
                    {
                        rsOutsourcedSupplierNew.Fields["MAINSUPPLIER"].Value = true;
                    }

                    dummy++;
                    Invoke(new MethodInvoker(delegate
                    {
                        label2.Text = $" Aanmaken nieuw uitbesteed werk inkoopprijzen {dummy} / {toCreateOutsourcedSuppliers.Count()}";
                    }));
                }

                Invoke(new MethodInvoker(delegate
                {
                    label2.Text = $"Aanmaken {toCreateOutsourcedSuppliers.Count()} nieuwe uitbesteed werk inkoopprijzen...";
                }));

                rsOutsourcedSupplierNew.MoveFirst();
                var resultInsertOutsourcedSuppliers = rsOutsourcedSupplierNew.Update2();

                Invoke(new MethodInvoker(delegate
                {
                    label2.Text = $"{toCreateOutsourcedSuppliers.Count()} nieuwe uitbesteed werk inkoopprijzen aangemaakt.";
                }));

                if (resultInsertOutsourcedSuppliers != null && resultInsertOutsourcedSuppliers.Any(x => x.HasError))
                {
                    MessageBox.Show(
                        $"Het aanmaken van inkoopprijzen uitbesteed werk is mislukt, oorzaak: {string.Join(";", resultInsertOutsourcedSuppliers.First(x => x.HasError).GetResult())}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                List<OutsourcedSupplier> createdOutsourcedSuppliers = rsOutsourcedSupplierNew.DataTable.AsEnumerable().Select(x => new OutsourcedSupplier()
                {
                    OutsourcedSupplierId = x.Field<int>("PK_R_OUTSOURCEDPURCHASEPRICE"),
                    SupplierId = x.Field<int>("FK_SUPPLIER"),
                    OutsourcedActivityId = x.Field<int>("FK_OUTSOURCEDACTIVITY"),
                    MainSupplier = x.Field<bool>("MAINSUPPLIER"),
                    Rate = x.Field<double>("RATE"),
                    TempPk = $"{x.Field<int>("FK_SUPPLIER")}_{x.Field<int>("FK_OUTSOURCEDACTIVITY")}",
                }).ToList();

                allOutsourcedSuppliers = allOutsourcedSuppliers.Concat(createdOutsourcedSuppliers).ToList();
            }



            //Ten slotte halen we alle aangepaste en aangemaakte uitbesteed werk en inkoopprijzen op en kijken we waar we de verkoopopslag moeten updaten.
            //Dit doen we hier omdat we de verkoopopslag alleen mogen updaten als de leverancier als hoofdleverancier is aangewezen
            var OutsourcedPriceInfo =
                from outsourcedActivity in outsourcedActivities
                join allOutsourcedSupplier in allOutsourcedSuppliers on outsourcedActivity.OutsourcedActivityId equals allOutsourcedSupplier.OutsourcedActivityId
                join OutsourcedSupplierExcel in OutsourcedSupplierExcelInfo on outsourcedActivity.OutsourcedActivityId equals OutsourcedSupplierExcel.OutSourcedActivityId
                select new
                {
                    OutsourcedActivityId = outsourcedActivity.OutsourcedActivityId,
                    MainSupplier = allOutsourcedSupplier.MainSupplier,
                    SalesMarkup = OutsourcedSupplierExcel.SalesMarkup,
                };

            var outsourcedSalesMarkupToUpdate = OutsourcedPriceInfo.Where(x => x.MainSupplier);

            if(outsourcedSalesMarkupToUpdate.Any())
            {
                Invoke(new MethodInvoker(delegate
                {
                    label2.Text =
                        $"{outsourcedSalesMarkupToUpdate.Count()} uitbesteed werk inkoopprijzen gevonden waarbij verkoopopslag bijgewerkt moet worden.";
                }));

                var rsOutsourcedActivitiesToUpdate = newSession.GetRecordsetColumns("R_OUTSOURCEDACTIVITY", "PK_R_OUTSOURCEDACTIVITY, SALESMARKUP",
                    $"PK_R_OUTSOURCEDACTIVITY IN ({string.Join(",", outsourcedSalesMarkupToUpdate.Select(x => x.OutsourcedActivityId))})");
                rsOutsourcedActivitiesToUpdate.UseDataChanges = false;
                rsOutsourcedActivitiesToUpdate.UpdateWhenMoveRecord = false;
                rsOutsourcedActivitiesToUpdate.MoveFirst();

                var dummy = 0;

                while (!rsOutsourcedActivitiesToUpdate.EOF)
                {
                    var outsourcedActivityToUpdateInfo = outsourcedSalesMarkupToUpdate.First(x => x.OutsourcedActivityId == (int)rsOutsourcedActivitiesToUpdate.GetField("PK_R_OUTSOURCEDACTIVITY").Value);

                    rsOutsourcedActivitiesToUpdate.SetFieldValue("SALESMARKUP", outsourcedActivityToUpdateInfo.SalesMarkup);

                    dummy++;
                    Invoke(new MethodInvoker(delegate
                    {
                        label2.Text =
                            $"Bijwerken verkoopopslag {dummy} / {outsourcedSalesMarkupToUpdate.Count()} uitbesteed werk.";
                    }));

                    rsOutsourcedActivitiesToUpdate.MoveNext();
                }

                Invoke(new MethodInvoker(delegate
                {
                    label2.Text = $"Opslaan {outsourcedSalesMarkupToUpdate.Count()} uitbesteed werk...";
                }));

                rsOutsourcedActivitiesToUpdate.MoveFirst();
                var resultUpdateOutsourcedActivities = rsOutsourcedActivitiesToUpdate.Update2();

                Invoke(new MethodInvoker(delegate
                {
                    label2.Text = $"{outsourcedSalesMarkupToUpdate.Count()} uitbesteed werk bijgewerkt.";
                }));

                if (resultUpdateOutsourcedActivities != null && resultUpdateOutsourcedActivities.Any(x => x.HasError))
                {
                    MessageBox.Show(
                        $"Het bijwerken van het verkoopopslag het uitbesteed werk is mislukt, oorzaak: {string.Join(";", resultUpdateOutsourcedActivities.First(x => x.HasError).GetResult())}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
        }

        public class ExcelRow
        {
            public string Itemcode { get; set; }
            public string Relationname { get; set; }
            public string PurchaseItemcode { get; set; }
            public string PurchaseDescription { get; set; }
            public string Itemgroup { get; set; }
            public string Itemunit { get; set; }
            public double Grossprice { get; set; }
            public double Discount { get; set; }
            public double Netprice { get; set; }
            public double SalesMarkup { get; set; }
        }


        public class OutsourcedActivitySalesMarkup
        {
            public int OutsourcedActivity { get; set; }
            public double SalesMarkup { get; set; }
        }

        public class Item
        {
            public int ItemId { get; set; }
            public string Code { get; set; }
        }

        public class ItemSupplier
        {
            public int ItemSupplierId { get; set; }
            public int ItemId { get; set; }
            public int SupplierId { get; set; }
            public string PurchaseItemCode { get; set; }
            public string PurchaseDescription { get; set; }
            public string TempPk { get; set; }
        }

        public class OutsourcedActivity
        {
            public int OutsourcedActivityId { get; set; }
            public string Code { get; set; }
            public double SalesMarkup { get; set; }
        }

        public class OutsourcedSupplier
        {
            public int OutsourcedSupplierId { get; set; }
            public int OutsourcedActivityId { get; set; }
            public int SupplierId { get; set; }
            public bool MainSupplier { get; set; }
            public double Rate { get; set; }
            public string TempPk { get; set; }
        }

        public class OutsourcedActivityUpdateSalesMarkup
        {
            public int OutsourcedActivityId { get; set; }
            public double SalesMarkup { get; set; }
        }

        // Back on the 'UI' thread so we can update the progress bar
        void BackgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            // The progress percentage is a property of e
            progressBar1.Value = e.ProgressPercentage;
        }

        void BackgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //Since this is executed on the main thread - it is not (as far as I know) going to "race" against the FormClosing.
            //Close();
        }

        void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (backgroundWorker1.IsBusy)
            {
                backgroundWorker1.CancelAsync();
                e.Cancel = true;
            }
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            Form1.ActiveForm.Close();
        }


    }
}

